package main

import (
	"fmt"
)

type Employee struct{
	Name string
	LastName string
	Age int
	Salary float32
	Experience int
}

type Manager struct{
	team map[string]string
	Employee
}

type Designer struct {
	effCoef float32
	Employee
}

type Developer struct{
	Employee
}

type IEmployee interface{
	setSalary() float32
}


func GetSalary(pos IEmployee, exp int) float32{
	var salary float32

	if exp >= 2 && exp < 5 {
		salary = pos.setSalary() + 200
	} else if exp >= 5 {
		salary = pos.setSalary() * 1.2 + 500
	} else {
		salary = pos.setSalary()
	}

	return salary
}

func (dev Developer) setSalary() float32{
	return dev.Salary

}

func (des Designer) setSalary() float32{
	return des.Salary * des.effCoef
}

func (man Manager) setSalary() float32{

	if len(man.team) > 5 && len(man.team) <= 10 {
		return man.Salary + 200
	}
	if len(man.team) > 10 {
		return man.Salary + 300
	}
	return man.Salary
}

func (emp Employee) setSalary() float32{
	return emp.Salary
}

func main(){
	var worker Employee
	var dev Developer
	var des Designer
	var man Manager

	worker.Name = "Denis"
	worker.LastName = "Kononenko"
	worker.Age = 23
	worker.Experience = 6
	worker.Salary = 5000

	dev.Name = "Stas"
	dev.Experience = 3
	dev.Salary = 4000

	des.Name = "Designer"
	des.effCoef = 0.5
	des.Salary = 1000
	des.Experience = 7

	man.Name = "Dmytro"
	man.Experience = 10
	man.Salary = 10000

	man.team = make(map[string]string)
	man.team[dev.Name] = "Dev"
	man.team[des.Name] = "Des"
	man.team["Lol"] = "Dev"



	fmt.Println(dev.Name, dev.LastName, dev.Experience, GetSalary(dev, dev.Experience))
	fmt.Println(des.Name, des.LastName, des.Experience, GetSalary(des, des.Experience))
	fmt.Println(worker.Name, worker.LastName, worker.Experience, GetSalary(worker, worker.Experience))
	fmt.Println(man.team, len(man.team))
}
